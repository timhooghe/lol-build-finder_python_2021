import requests
from getLatestPatchV import patch

# bij nieuwe champ release eventueel getal in deze URL updaten (evt getal in variabele steken
# om op te halen --> https://ddragon.leagueoflegends.com/api/versions.json)
response = requests.get('http://ddragon.leagueoflegends.com/cdn/' + patch + '/data/en_US/champion.json')

if response:
    print('Success!')
else:
    print('An error has occurred.')

json_response = response.json()
champions = json_response['data']

champNames = []

for i in champions:
    champNames.append(champions[i]["id"].lower())
