import tkinter as tk
from tkinter import *
import webbrowser
from champlistAPI import champNames

#main
window = tk.Tk()
window.title("League of Legends Build finder By Tim Hooghe")
window.iconbitmap("images/lol.ico")
window.config(bg="black")
window.resizable(False, False)

#images
image1 = PhotoImage(file="images/qiyana.gif")
image2 = PhotoImage(file="images/searchicon2.png")

#functie van de button (mobalytics build page van ingetypte champ openen in browser)
def search():
    zoekopdracht = searchBar.get().lower()
    if zoekopdracht in champNames:
        webbrowser.open("https://app.mobalytics.gg/lol/champions/" + zoekopdracht + "/build")
    else:
        print("This champion does not exist.")

#eerste label (vervang dit door afbeelding)
testje = tk.Label(image=image1, bg="black")
testje.pack()

#tweede label (uitleggen wat user moet doen)
mainPhrase = tk.Label(text="Enter a league of legends champion name ...", font="verdana 12 bold", fg="white", bg="black")
mainPhrase.pack()

#derde label (hier moet de user zijn zoekopdracht typen)
searchBar = tk.Entry(font="verdana", width=12)
searchBar.pack(pady=8)

#button
searchButton = tk.Button(image=image2, height=28, width=28, borderwidth=0, bg="black", activebackground="black", command=search)
searchButton.pack(pady=(5,15))

#belangrijk: zonder deze line werkt programma niet
window.mainloop()